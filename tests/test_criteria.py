import tomli

from mcasopt.criteria import parse_criteria
from mcasopt.results import read_results


def test_criteria_1():
    crit_toml = """
    [[compound_state]]
    weight = 1.0
    energy = 1.3
    spin = ["1/2+"]
    weight_half_width = 1.0
    half_width = 0.38

    [[compound_state]]
    weight = 1.0
    energy = 2.7
    spin = ["5/2"]
    weight_half_width = 0.5
    half_width = 0.17

    [[compound_state]]
    weight = 0.25
    energy = 4.9
    spin = ["1/2-", "3/2-", "5/2-"]
    """

    crit_dict = tomli.loads(crit_toml)
    crit = parse_criteria(crit_dict['compound_state'])

    res_lines_1 = [
        '      1.2791   1/2+  3.5401D-01   1 0.5\n',
        '      2.7545   5/2+  1.6793D-01   1 0.5\n',
        '      4.7554   1/2-  5.3426D-02   1 0.5\n',
        '      6.1476   3/2-  1.4476D-01   1 0.5\n',
        '      6.3361   1/2-  2.5097D-01   1 0.5\n',
    ]
    res_1 = read_results(res_lines_1)

    res_lines_2 = [
        '      1.3   1/2+  3.8000D-01   1 0.5\n',
        '      2.7   5/2+  1.7000D-01   1 0.5\n',
        '      4.9   1/2-  5.3426D-02   1 0.5\n',
    ]
    res_2 = read_results(res_lines_2)

    assert crit.score(res_1) > 0
    assert crit.score(res_2) == 0


def test_criteria_nearest_matches():
    crit_toml = """
    [[compound_state]]
    weight = 1.0
    energy = -1.0
    spin = ["5/2+"]

    [[compound_state]]
    weight = 1.0
    energy = 1.0
    spin = ["1/2+"]

    [[compound_state]]
    weight = 1.0
    energy = 2.0
    spin = []

    [[compound_state]]
    weight = 1.0
    energy = 3.0
    spin = ["1/2-", "3/2-", "5/2-"]
    """

    crit_dict = tomli.loads(crit_toml)
    crit = parse_criteria(crit_dict['compound_state'])

    # Perfectly-matching results, with some intruder states.
    perfect = read_results(
        [
            '     -1.0000   5/2+  1.0000D-01   1 0.5\n',
            '      0.5000   5/2+  1.0000D-01   1 0.5\n',
            '      1.0000   1/2+  1.0000D-01   1 0.5\n',
            '      1.1000   1/2+  1.0000D-01   1 0.5\n',
            '      1.9000   5/2-  1.0000D-01   1 0.5\n',
            '      2.0000   7/2-  1.0000D-01   1 0.5\n',
            '      3.0000   3/2-  1.0000D-01   1 0.5\n',
            '      4.0000   1/2-  1.0000D-01   1 0.5\n',
        ]
    )

    score = crit.score(perfect)
    assert score == 0.0

    # Imperfectly-matching results, with some intruder states.
    imperfect = read_results(
        [
            '     -1.0000   5/2+  1.0000D-01   1 0.5\n',
            '      0.5000   5/2+  1.0000D-01   1 0.5\n',
            '      1.0000   3/2+  1.0000D-01   1 0.5\n',
            '      1.1000   1/2+  1.0000D-01   1 0.5\n',  # Error of 0.1.
            '      1.9000   5/2-  1.0000D-01   1 0.5\n',
            '      2.0000   7/2-  1.0000D-01   1 0.5\n',
            '      3.0000   7/2-  1.0000D-01   1 0.5\n',
            '      4.0000   1/2-  1.0000D-01   1 0.5\n',  # Error of 1.0.
        ]
    )

    score = crit.score(imperfect)
    assert score == 1.1

    # Imperfectly-matching results, with some intruder states.
    imperfect = read_results(
        [
            '     -1.0000   3/2+  1.0000D-01   1 0.5\n',
            '      0.5000   5/2+  1.0000D-01   1 0.5\n',  # Error of 1.5.
            '      1.0000   3/2+  1.0000D-01   1 0.5\n',
            '      1.1000   3/2+  1.0000D-01   1 0.5\n',  # Error of 0.9.
            '      1.9000   1/2+  1.0000D-01   1 0.5\n',  # Error of 0.9.
            '      3.0000   7/2-  1.0000D-01   1 0.5\n',
            '      4.0000   1/2-  1.0000D-01   1 0.5\n',  # Error of 1.0.
        ]
    )

    score = crit.score(imperfect)
    assert score == 4.3
