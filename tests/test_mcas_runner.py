from pathlib import Path
import shutil

import mcasopt
import mcasopt.example


def test_mcas_runner():
    def input_fun(mcas_input, x):
        return mcas_input

    out_dir = Path('doc').resolve() / 'src' / '_static'

    mcasopt.example.save_n_plus_12C_files()
    config_file = 'n+12C.toml'
    config = mcasopt.load_config(config_file)

    x_0 = [-47, -45]
    evaluator = mcasopt.McasEvaluator.run_with_slurm(config, input_fun)(x_0)

    job_id = '_test_mcas_runner'
    mcas_cmd = evaluator.command(job_id)
    evaluator.runner.write_script(out_dir / 'example_job.sh', mcas_cmd)

    mcasopt.example.remove_n_plus_12C_files()
    exp_dir = evaluator.experiment_dir()
    shutil.rmtree(exp_dir)
