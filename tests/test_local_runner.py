from pathlib import Path
import time

import mcasopt.run


def test_local_runner_success():
    command = mcasopt.run.Command(
        args=['sleep', '2s'],
        stdin=Path('/dev/null'),
        stdout=Path('/dev/null'),
        cwd=Path('.'),
    )
    runner = mcasopt.run.LocalRunner()
    job = runner.start_command(command)
    assert runner.completion_status(job) is None
    for _ in range(5):
        time.sleep(1)
        if runner.completion_status(job) is not None:
            break
    assert runner.completion_status(job) == 0


def test_local_runner_failure():
    command = mcasopt.run.Command(
        args=['cmp', '--silent', 'Makefile', 'README.rst'],
        stdin=None,
        stdout=None,
        cwd=None,
    )
    runner = mcasopt.run.LocalRunner()
    job = runner.start_command(command)
    for _ in range(5):
        time.sleep(1)
        if runner.completion_status(job) is not None:
            break
    assert runner.completion_status(job) != 0
