import logging
from pathlib import Path
import shutil
import numpy as np

from mcasopt import minimise, load_config, McasEvaluator
from mcasopt.run import FakeMcasRunner
from mcasopt.example import save_n_plus_12C_files, remove_n_plus_12C_files


def test_fake_mcas(caplog):
    """
    Use ``mcasopt.run.fake_mcas`` to check that the solver finds the expected
    solution.
    """
    caplog.set_level(logging.INFO)

    save_n_plus_12C_files()
    config = load_config('n+12C.toml')
    config.solver.sleep_duration = 0

    def input_fun(mcas_input, x):
        # Adjust the simulation settings according to the values in x.
        mcas_input.model.v0 = [x[0], 0.0, x[0]]
        mcas_input.model.vll = [x[1], 0.0, x[1]]
        return mcas_input

    # Start near the true solution of [-34.16, 0.475].
    x_0 = [-34.0, 0.5]

    # Ensure we start with an empty cache.
    config.solver.cache_file.unlink(missing_ok=True)

    # Use the fake-MCAS runner.
    runner = FakeMcasRunner()

    # Start the solver and check that we obtain the expected solution.
    evaluator = McasEvaluator.builder(config, runner, input_fun)
    result = minimise(evaluator, x_0, config.solver)
    assert np.allclose(result.x, np.array([-34.16, 0.475]), rtol=1e-3)

    # Ensure that all results were obtained by evaluating the function.
    uncached_results_1 = [
        record
        for record in caplog.records
        if record.message.startswith('Evaluating at x = ')
    ]
    cached_results_1 = [
        record
        for record in caplog.records
        if record.message.startswith('Using cached value ')
    ]
    # NOTE: it is possible that the solver will evaluate the function more
    # than once at the same location in parameter space.
    assert len(uncached_results_1) > len(cached_results_1)

    # Run the solver a second time, it should use cached results.
    result = minimise(evaluator, x_0, config.solver)
    assert np.allclose(result.x, np.array([-34.16, 0.475]), rtol=1e-3)

    # Ensure that all results were obtained from the cache.
    uncached_results_2 = [
        record
        for record in caplog.records
        if record.message.startswith('Evaluating at x = ')
    ]
    cached_results_2 = [
        record
        for record in caplog.records
        if record.message.startswith('Using cached value ')
    ]

    # This second pass should not have performed any evaluations.
    assert len(uncached_results_2) == len(uncached_results_1)
    # This second pass should have used a cached evaluation for all of the
    # evaluations in the first pass.
    assert len(cached_results_2) == (
        # Evaluations that were retrieved from cache in pass #1.
        len(cached_results_1)
        # Pass #1 evaluations that were retrieved from cache in pass #2.
        + len(uncached_results_1)
        + len(cached_results_1)
    )

    # Clean up.
    config.solver.cache_file.unlink(missing_ok=True)
    remove_n_plus_12C_files()
    cwd = Path.cwd()
    exp_dir = config.experiment.name + '_x'
    shutil.rmtree(cwd / exp_dir, ignore_errors=True)
