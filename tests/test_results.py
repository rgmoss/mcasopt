import io

from mcasopt.results import read_results, CompoundState


def test_read_results():
    content = io.StringIO(
        """
    #       Bound and Resonance states List
    #    Energy    J/Pi  Half-width   x dx
    #
         -4.8566   1/2-     0.0       1 0.5
         -1.9873   1/2+     0.0       1 0.5
         -1.7995   5/2+     0.0       1 0.5
         -1.4044   3/2-     0.0       1 0.5
    #
          0.1295   5/2-  3.2124D-10   1 0.5
          2.0006   5/2+  1.0484D-02   1 0.5
          2.6612   7/2+  9.4963D-07   1 0.5
          2.7112   1/2-  9.2245D-04   1 0.5
          2.7795   3/2+  3.8467D-02   1 0.5
          3.3042   3/2+  4.7127D-01   1 0.5
    #
    ########################################################
    #
    # With XMGRACE ---
    #     1) use option `blockdata
    #     2) select x-y-dx form
    #     3) enter x from col 4
    #     4) enter y from col 1
    #     5) enter y1 from col 5

    """
    )

    states = read_results(content)

    # Ensure that 10 compound states have been returned.
    assert len(states) == 10
    for state in states:
        assert isinstance(state, CompoundState)

    # Check the values of each compound state.
    assert states[0].energy == -4.8566
    assert states[0].spin.num == 1
    assert states[0].spin.denom == 2
    assert states[0].spin.sign == '-'
    assert states[0].half_width == 0.0

    assert states[1].energy == -1.9873
    assert states[1].spin.num == 1
    assert states[1].spin.denom == 2
    assert states[1].spin.sign == '+'
    assert states[1].half_width == 0.0

    assert states[2].energy == -1.7995
    assert states[2].spin.num == 5
    assert states[2].spin.denom == 2
    assert states[2].spin.sign == '+'
    assert states[2].half_width == 0.0

    assert states[3].energy == -1.4044
    assert states[3].spin.num == 3
    assert states[3].spin.denom == 2
    assert states[3].spin.sign == '-'
    assert states[3].half_width == 0.0

    assert states[4].energy == 0.1295
    assert states[4].spin.num == 5
    assert states[4].spin.denom == 2
    assert states[4].spin.sign == '-'
    assert states[4].half_width == 3.2124e-10

    assert states[5].energy == 2.0006
    assert states[5].spin.num == 5
    assert states[5].spin.denom == 2
    assert states[5].spin.sign == '+'
    assert states[5].half_width == 1.0484e-2

    assert states[6].energy == 2.6612
    assert states[6].spin.num == 7
    assert states[6].spin.denom == 2
    assert states[6].spin.sign == '+'
    assert states[6].half_width == 9.4963e-7

    assert states[7].energy == 2.7112
    assert states[7].spin.num == 1
    assert states[7].spin.denom == 2
    assert states[7].spin.sign == '-'
    assert states[7].half_width == 9.2245e-4

    assert states[8].energy == 2.7795
    assert states[8].spin.num == 3
    assert states[8].spin.denom == 2
    assert states[8].spin.sign == '+'
    assert states[8].half_width == 3.8467e-2

    assert states[9].energy == 3.3042
    assert states[9].spin.num == 3
    assert states[9].spin.denom == 2
    assert states[9].spin.sign == '+'
    assert states[9].half_width == 4.7127e-1
