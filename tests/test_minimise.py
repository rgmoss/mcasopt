"""Test cases for the mcasopt.minimise module."""

import numpy as np
from mcasopt.solver import (
    Solver,
    Builder,
    FunctionEvaluator,
    ExpressionEvaluator,
    minimise,
)


def simple_fun(x, counter=None):
    """Define a function of two variables."""
    return abs((x[0] - 2) ** 3) + (x[1] - 4) ** 2


def expr_fun(x, counter=None):
    """Define an expression of two variables."""
    return 'abs({} - 2)**3 + ({} - 4)**2'.format(x[0], x[1])


def test_evaluating_functions_and_expressions():
    """
    Ensure we obtain identical results when evaluating a Python function, and
    when evaluating the same numeric expression in a separate Python process.
    """

    config_solver = Solver(
        method='BFGS',  # 'Nelder-Mead'
        cache_file='cache.dat',
        sleep_duration=0,
        log_level='INFO',
        epsilon=1.5e-8,
    )
    x_0 = [1, 3]

    eval_fun: Builder = FunctionEvaluator.builder(simple_fun)
    eval_expr: Builder = ExpressionEvaluator.builder(expr_fun)

    config_solver.cache_file.unlink(missing_ok=True)
    result_fun = minimise(eval_fun, x_0, config_solver)

    config_solver.cache_file.unlink(missing_ok=True)
    result_expr = minimise(eval_expr, x_0, config_solver)

    config_solver.cache_file.unlink(missing_ok=True)

    if result_fun.success and result_expr.success:
        assert np.allclose(result_fun.x, result_expr.x)
        assert result_fun.nfev == result_expr.nfev
    else:
        print(result_fun.message)
        print(result_expr.message)
        message = f'Func: {result_fun.message}\nExpr: {result_expr.message}'
        raise AssertionError(message)
