mcasopt
=======

|docs| |tests| |coverage|

The mcasopt_ Python package uses numerical optimisation routines to find
approximately-optimal parameter values for MCAS, given a desired set of output
resonances.

It is distributed under the terms of the `MIT License`_ (see ``LICENSE``).

.. _mcasopt: http://bitbucket.org/robmoss/mcasopt
.. _MIT License: https://choosealicense.com/licenses/mit/

.. |docs| image::  https://readthedocs.org/projects/mcasopt/badge/
   :alt: Documentation
   :target: https://mcasopt.readthedocs.io/

.. |tests| image:: https://gitlab.unimelb.edu.au/rgmoss/mcasopt/badges/master/pipeline.svg
   :alt: Test cases
   :target: https://gitlab.unimelb.edu.au/rgmoss/mcasopt

.. |coverage| image:: https://gitlab.unimelb.edu.au/rgmoss/mcasopt/badges/master/coverage.svg
   :alt: Test coverage
   :target: https://gitlab.unimelb.edu.au/rgmoss/mcasopt
