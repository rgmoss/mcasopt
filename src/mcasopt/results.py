"""Parse result files."""

import re


class Spin(object):
    _spin_re = re.compile(r'(\d+)/(\d+)([+-])')

    def __init__(self, str_val):
        m = re.match(self._spin_re, str_val)
        if m is None:
            raise ValueError("invalid spin '{}'".format(str_val))
        self.num = int(m.group(1))
        self.denom = int(m.group(2))
        self.sign = m.group(3)

    def __str__(self):  # pragma: no cover
        return '{}/{}{}'.format(self.num, self.denom, self.sign)

    def __repr__(self):  # pragma: no cover
        return self.__str__()


class CompoundState(object):
    def __init__(self, line):
        cols = line.split()
        if len(cols) < 3:
            raise ValueError("fewer than 3 columns in '{}'".format(line))
        # Note: retain the string value for the # of significant digits.
        self.energy_str = cols[0]
        self.energy = float(cols[0])
        self.spin = Spin(cols[1])
        self.half_width = float(cols[2].replace('D', 'E').replace('d', 'e'))

    def __str__(self):  # pragma: no cover
        return '{}  {}  {}'.format(self.energy, self.spin, self.half_width)

    def __repr__(self):  # pragma: no cover
        return self.__str__()


def read_results(f):
    """
    Read the results of a model simulation.

    :param f: A file-like object from which to read the results.
    """
    results = []
    for line in f:
        # Ignore comments.
        line = line.strip()
        if line.startswith('#'):
            continue
        elif line:
            results.append(CompoundState(line))
    return results
