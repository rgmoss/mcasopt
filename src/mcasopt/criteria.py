"""
Compare compound states to (possibly imprecise) criteria.

Each criterion comprises the following:

* A weight [0..1], used to scale the objective function.
* A half-width weight [0..1], used to scale the relative contribution of the
  energy and half-width errors.
* An energy value.
* An *optional* half-width value; if this is not specified, the half-width
  error is defined to be *zero*.
* A list of zero or more spin values, each of which may or may not include a
  sign.

Example::

These criteria should be defined in a TOML file as an array of tables:

.. code-block:: toml

   [[compound_state]]
   weight = 1
   energy = 0.1295
   spin = ["5/2-", "3/2-"]
   weight_half_width = 1
   half_width = 3.2125e-10
"""

import logging
import re


class Spin:
    def __init__(self, num, denom, sign=None):
        self.num = num
        self.denom = denom
        self.sign = sign

    def is_match(self, state):
        if self.num != state.spin.num:
            return False
        if self.denom != state.spin.denom:
            return False
        if self.sign is not None and self.sign != state.spin.sign:
            return False
        return True

    def __str__(self):
        if self.sign is None:
            return '{}/{}'.format(self.num, self.denom)
        else:
            return '{}/{}{}'.format(self.num, self.denom, self.sign)

    def __repr__(self):  # pragma: no cover
        return self.__str__()

    def __eq__(self, other):
        if not isinstance(other, Spin):
            return False
        return (
            self.num == other.num
            and self.denom == other.denom
            and self.sign == other.sign
        )


class Criterion:
    def __init__(self, weight, energy, spin, weight_hw=0, half_width=None):
        self.weight = weight
        self.weight_hw = weight_hw
        self.energy = energy
        if isinstance(spin, Spin):
            self.spin = [spin]
        else:
            self.spin = spin
        self.half_width = half_width

    def energy_error(self, state):
        return abs(self.energy - state.energy)

    def half_width_error(self, state):
        if state.energy < 0 and state.half_width == 0.0:
            # Bound states have zero half-width, and so we should behave as
            # though there isn't a half_width criterion.
            return 0.0
        if self.half_width is not None:
            return abs(self.half_width - state.half_width)
        else:
            # No error if there isn't a half_width criterion.
            return 0.0

    def spin_ok(self, state):
        # Check against one or more, possibly imprecise, spins.
        if not self.spin:
            return True
        else:
            return any(s.is_match(state) for s in self.spin)

    def score(self, state, spin_penalty):
        logger = logging.getLogger(__name__)
        # Return a (weighted) objective score for this state.
        err_e = self.energy_error(state)
        err_hw = self.half_width_error(state)
        err = self.weight * (err_e + self.weight_hw * err_hw)
        logger.debug('Criteria is {}'.format(str(self)))
        logger.debug(
            'Error is {} for {} and {}'.format(
                err, state.energy, state.half_width
            )
        )

        if self.spin_ok(state):
            return err
        else:
            # Add a massive penalty for not getting the spin correct.
            return err + spin_penalty

    def __str__(self):
        # Format spin criteria as they appear in criteria files.
        spin_str = '[' + ' '.join(str(s) for s in self.spin) + ']'
        if self.half_width is None:
            return '{} {} {}'.format(self.weight, self.energy, spin_str)
        else:
            return '{} {} {} {} {} '.format(
                self.weight,
                self.energy,
                spin_str,
                self.weight_hw,
                self.half_width,
            )

    def __repr__(self):  # pragma: no cover
        return self.__str__()

    def __eq__(self, other):
        if not isinstance(other, Criterion):
            return False
        return (
            self.weight == other.weight
            and self.weight_hw == other.weight_hw
            and self.energy == other.energy
            and self.spin == other.spin
            and self.half_width == other.half_width
        )


def find_closest_state(criterion, states):
    """
    Find the compound state that (a) satisfies the spin and parity criterion,
    and (b) has the smallest different in energy to the criterion.

    Return ``(closest_state, other_states)`` if such a compound state exists,
    otherwise returns ``(None, states)``.
    """
    candidates = list(states)
    closest_ix = None
    smallest_error = None
    for ix, state in enumerate(candidates):
        if not criterion.spin_ok(state):
            continue
        error = criterion.energy_error(state)
        if smallest_error is None or error < smallest_error:
            smallest_error = error
            closest_ix = ix
    if smallest_error is None:
        # No matching states were found.
        return (None, states)
    else:
        closest_state = candidates.pop(closest_ix)
        return (closest_state, candidates)


class Criteria:
    def __init__(self, criteria):
        self.criteria = criteria

    def ordered_criteria(self):
        """
        Sort the criteria from most restrictive (having a single spin and
        parity) to less restrictive (having multiple spins and/or parities) to
        least restrictive (having no spin and parity).
        """
        crits_no_spin = [c for c in self.criteria if len(c.spin) == 0]
        crits_one_spin = [c for c in self.criteria if len(c.spin) == 1]
        crits_multi_spin = [c for c in self.criteria if len(c.spin) > 1]
        return crits_one_spin + crits_multi_spin + crits_no_spin

    def score(self, states, penalty_spin=1e6, penalty_missing=1e6):
        if len(states) < len(self.criteria):
            # Penalise missing states similar to non-matching spins.
            num_missing = len(self.criteria) - len(states)
            penalty = num_missing * penalty_missing
        else:
            penalty = 0
        # Sort the criteria so that we match the most restrictive ones first.
        ordered_criteria = self.ordered_criteria()
        candidates = [state for state in states]
        # Score how closely the output states match each criterion.
        scores = []
        for crit in ordered_criteria:
            closest_state, candidates = find_closest_state(crit, candidates)
            if closest_state is not None:
                scores.append(crit.score(closest_state, penalty_spin))
        score = sum(scores)
        return score + penalty

    def __str__(self):
        return '\n'.join(str(c) for c in self.criteria)

    def __repr__(self):  # pragma: no cover
        return self.__str__()

    def __eq__(self, other):
        if not isinstance(other, Criteria):
            return False
        return self.criteria == other.criteria


_spin_re = re.compile(r'(\d+)/(\d+)([+-]?)')


def parse_spin(str_val):
    m = re.match(_spin_re, str_val)
    if m is None:
        raise ValueError("invalid spin criteria '{}'".format(str_val))
    num = int(m.group(1))
    denom = int(m.group(2))
    sign = m.group(3)
    if not sign:
        # NOTE: replace matching empty strings with None.
        sign = None
    return Spin(num, denom, sign)


def parse_criteria(criteria):
    """
    Parse model criteria and return a `Criteria` instance.
    """
    crits = []
    for criterion in criteria:
        weight = criterion['weight']
        energy = criterion['energy']
        spin = [parse_spin(spin) for spin in criterion['spin']]
        weight_hw = criterion.get('weight_half_width', 0)
        half_width = criterion.get('half_width')
        crit = Criterion(weight, energy, spin, weight_hw, half_width)
        crits.append(crit)
    return Criteria(crits)
