"""
Create templates for MCAS experiments.
"""

import abc
from pathlib import Path
import re
import sys
import textwrap
import typing


T = typing.TypeVar('T', covariant=True)


class Validator(typing.Protocol[T]):
    @abc.abstractmethod
    def parse_response(self, value: str) -> typing.Optional[T]:
        raise NotImplementedError

    def do_retry(self) -> bool:
        return True

    @abc.abstractmethod
    def retry_message(self, value: str) -> str:
        pass


class PathValidator(Validator[Path]):
    def __init__(self, basename: typing.Optional[str] = None):
        self.basename = basename

    def parse_response(self, value: str) -> typing.Optional[Path]:
        path = Path(value)
        try:
            path = path.resolve(strict=True)
        except FileNotFoundError:
            return None
        if self.basename is not None:
            if path.name != self.basename:
                if path.is_dir():
                    # Check if the file exists in this directory.
                    sub_path = path / self.basename
                    if sub_path.exists():
                        return sub_path
                return None
        return path

    def retry_message(self, value: str) -> str:
        return f'File not found or invalid: "{value}"'


class VenvValidator(Validator[Path]):
    def parse_response(self, value: str) -> typing.Optional[Path]:
        path = Path(value)
        try:
            path = path.resolve(strict=True)
        except FileNotFoundError:
            return None
        activate = path / 'bin' / 'activate'
        if not activate.exists():
            return None
        return path

    def retry_message(self, value: str) -> str:
        return f'Venv not found or invalid: "{value}"'


class RegexValidator(Validator[str]):
    def __init__(self, pattern: typing.Pattern[str]):
        self.pattern = pattern

    def parse_response(self, value: str) -> typing.Optional[str]:
        if self.pattern.match(value):
            return value
        else:
            return None

    def retry_message(self, value: str) -> str:
        return f'Invalid response: "{value}"'


class BoolValidator(Validator[bool]):
    def parse_response(self, value: str) -> typing.Optional[bool]:
        if value.lower() in ['y', 'yes']:
            return True
        elif value.lower() in ['n', 'no']:
            return False
        else:
            return None

    def retry_message(self, value: str) -> str:
        return f'Invalid response: "{value}"'


def ask_user(prompt: str, validator: Validator[T]) -> typing.Optional[T]:
    while True:
        response = input(prompt)
        result = validator.parse_response(response)
        if result is not None:
            return result
        elif not validator.do_retry():
            return None
        else:
            print(validator.retry_message(response))
            print()


def ask_many(prompts):
    return {
        name: ask_user(prompt, validator)
        for (name, (prompt, validator)) in prompts.items()
    }


def create_files(inputs):
    cwd = Path.cwd()
    name = inputs['name']
    venv_dir = inputs['venv']
    if inputs['mcas'].is_relative_to(cwd):
        mcas = inputs['mcas'].relative_to(cwd)
    else:
        mcas = inputs['mcas']
    if inputs['amdc'].is_relative_to(cwd):
        amdc = inputs['amdc'].relative_to(cwd)
    else:
        amdc = inputs['amdc']

    # Ensure the experiment directory exists or is created.
    if inputs['subdir']:
        exp_dir = Path(name).resolve()
        exp_dir.mkdir(parents=True, exist_ok=True)
    else:
        exp_dir = cwd.resolve()

    print(f'Creating files in {exp_dir} ...')

    # Create the experiment job script.
    sh_file = exp_dir / f'{name}.sh'
    print(f'Creating {sh_file.name} ...')
    sh_content = f"""\
    #!/bin/bash
    #SBATCH --partition=cascade
    #SBATCH --time=30-00:00:00
    #SBATCH --ntasks=1
    #SBATCH --job-name={name}
    #SBATCH --output={name}.log
    module load foss
    module load SciPy-bundle
    source {venv_dir}/bin/activate
    python3 -m mcasopt {name}.toml
    """
    with open(sh_file, 'w') as f:
        f.write(textwrap.dedent(sh_content))
    # Ensure the file is user-executable.
    sh_file.chmod(0o764)

    # Create the input function Python file.
    py_file = exp_dir / f'{name}.py'
    py_content = """\
    # Return the MCAS input for parameter vector ``x``.
    def input_fun(mcas_input, x):
        # Adjust the MCAS input according to the values in ``x``.
        mcas_input.model.v0 = [x[0], 0.0, x[1]]
        return mcas_input
    """
    with open(py_file, 'w') as f:
        f.write(textwrap.dedent(py_content))
    print(f'You MUST EDIT {py_file.name}')

    # Create the mcasopt experiment file.
    exp_file = exp_dir / f'{name}.toml'
    exp_content = f"""\
    [experiment]
    name = "{name}"
    input_file = "{name}.inp"
    mcas_path = "{mcas}"
    amdc_masses_path = "{amdc}"
    input_function = "{name}.input_fun"
    x_0 = [-49, -47.1]
    runner = "slurm"
    backup_results = true

    [solver]
    method = "Nelder-Mead"
    sleep_duration = 10
    cache_file = "{name}.cache"
    log_level = "INFO"
    epsilon = 1e-3

    [slurm]
    shebang = "#!/bin/bash"
    header.partition = "cascade"
    header.time = "30-00:00:00"
    header.ntasks = "1"
    header.job-name = "{name}-job"
    header.output = "{name}-job.log"
    modules = ["iompi"]

    # First resonance
    [[compound_state]]
    weight = 1.0
    energy = 0.1295
    spin = ["5/2-"]
    weight_half_width = 1.0
    half_width = 3.2125e-10

    # Second resonance
    [[compound_state]]
    weight = 1.0
    energy = 2.0006
    spin = ["5/2+"]
    weight_half_width = 1.0
    half_width = 1.0484e-2

    # Third resonance
    [[compound_state]]
    weight = 1.0
    energy = 2.6612
    spin = ["7/2+"]
    weight_half_width = 1.0
    half_width = 9.4961e-7
    """
    with open(exp_file, 'w') as f:
        f.write(textwrap.dedent(exp_content))
    print(f'You MUST EDIT {exp_file.name}')

    # Tell user to create the MCAS input file.
    inp_file = exp_dir / f'{name}.inp'
    print(f'You MUST CREATE {inp_file.name}')


def main(args=None):
    name_re = re.compile('^[-+_a-zA-Z0-9]+$')
    prompts = {
        'mcas': ('Path to mcas binary: ', PathValidator('mcas5.4')),
        'amdc': (
            'Path to AMDC-masses.dat: ',
            PathValidator('AMDC-masses.dat'),
        ),
        'venv': ('Path to mcasopt virtual environment: ', VenvValidator()),
        'name': ('Experiment name: ', RegexValidator(name_re)),
        'subdir': ('Create a new directory [y/n]: ', BoolValidator()),
    }
    inputs = ask_many(prompts)
    create_files(inputs)
    return 0


if __name__ == '__main__':
    sys.exit(main())
