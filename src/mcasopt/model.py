"""
Read and write MCAS input files.

Example:

    >>> from mcasopt.example import save_n_plus_12C_files
    >>> from mcasopt.model import load_input
    >>>
    >>> # Write the 'n+12C' files to the working directory.
    >>> save_n_plus_12C_files()
    >>>
    >>> # Load the 'n+12C' input file.
    >>> mcas_input = load_input('example-n+12C.inp')
    >>>
    >>> # Inspect the fileinfo block.
    >>> mcas_input.fileinfo
    &fileinfo
     runname = 'example-n+12C'
     options = 'sturm,rotation,res,fast'
    &end

    >>> # Inspect the first 'targex' element, using **zero-based** indexing.
    >>> mcas_input.model.targex[0]
     targex(1)%energy = 0.0
     targex(1)%spin = 0.0
     targex(1)%parity = 1
     targex(1)%alla = 0.0, 1000000.0, 1000000.0

    >>> # Modify the spin and inspect the results.
    >>> mcas_input.model.targex[0].spin = 2.0
    >>> mcas_input.model.targex[0]
     targex(1)%energy = 0.0
     targex(1)%spin = 2.0
     targex(1)%parity = 1
     targex(1)%alla = 0.0, 1000000.0, 1000000.0

    >>> # Write the modified settings to disk.
    >>> mcas_input.write_to('modified-n+12C.inp')
    >>>
    >>> # Read the new settings.
    >>> new_input = load_input('modified-n+12C.inp')
    >>> assert new_input == mcas_input
"""

import copy
from io import StringIO
import re


# Regular expressions that define the format of lines in the input files.
_struct_re = re.compile(r'^([a-z]+)\((\d+)\)%([a-z_]+)')
_value_re = re.compile(r'^(.+)\s*=\s*(.+)$')
_block_re = re.compile(r'^&([a-z]+)$')
_end_re = re.compile(r'^&end$')


def _parse_numeric_val(str_val):
    """
    Convert a string into an integer or floating-point value, as appropriate.
    """
    if str_val.isdigit():
        return int(str_val)
    else:
        return float(str_val.replace('D', 'E').replace('d', 'e'))


def _format_numeric_val(value, prec=3):
    """
    Convert an integer of floating-point value into a string.
    """
    if isinstance(value, int):
        return value
    float_str = str(value)
    if 'e' in float_str:
        float_str = '{:.{prec}e}'.format(value, prec=prec).replace('e', 'd')
        float_str = float_str.replace('+', '').replace('d00', 'd0')
    return float_str


def parse_block_name(line):
    m = re.match(_block_re, line)
    if m is None:
        return None
    else:
        return m.group(1)


def parse_value(line):
    m = re.match(_value_re, line)
    if m is None:
        raise ValueError("invalid line '{}'".format(line))
    name = m.group(1).strip()
    n_match = re.match(_struct_re, name)
    if n_match is not None:
        name = (n_match.group(1), int(n_match.group(2)), n_match.group(3))
    value = m.group(2).strip()
    if value.startswith("'") and value.endswith("'"):
        # String value, remove the surrounding quotes.
        value = value[1:-1]
    elif ',' in value:
        # Comma-separated list of numbers
        value = [_parse_numeric_val(v) for v in value.split(',')]
    else:
        value = _parse_numeric_val(value)
    return name, value


def parse_block(f):
    # Jump to the start of the next block.
    for line in f:
        line = line.strip()
        block_name = parse_block_name(line)
        if block_name is not None:
            break
    else:
        # No block to read.
        return None
    # Read this block.
    values = {}
    for line in f:
        line = line.strip()
        if re.match(_end_re, line):
            break
        if line:
            name, value = parse_value(line)
            values[name] = value
    else:
        raise ValueError('block {} has no end'.format(block_name))
    # Return the block name and the dictionary of values.
    return block_name, values


def format_value(value):
    if isinstance(value, str):
        return f"'{value}'"
    elif isinstance(value, float):
        return _format_numeric_val(value)
    elif isinstance(value, int):
        return str(value)
    elif isinstance(value, list):
        return ', '.join(format_value(v) for v in value)
    else:
        raise ValueError(f'unsupported parameter type {type(value)}')


class SubBlock:
    def __init__(self, name, nth):
        self._name = name
        self._nth = nth
        self._parameter_names = []

    def add_parameter(self, name, value):
        setattr(self, name, value)
        self._parameter_names.append(name)

    def parameters(self):
        for name in self._parameter_names:
            full_name = f'{self._name}({self._nth})%{name}'
            yield (full_name, getattr(self, name))

    def parameter_lines(self):
        for full_name, value in self.parameters():
            yield f' {full_name} = {format_value(value)}\n'

    def __eq__(self, other):
        if type(other) != type(self):
            return False
        return self.__dict__ == other.__dict__

    def __str__(self):
        return ''.join(self.parameter_lines())

    def __repr__(self):  # pragma: no cover
        return self.__str__()


class Block:
    def __init__(self, block_name, parameters):
        self._name = block_name
        self._parameter_names = []
        self._subblock_names = []
        for name, value in parameters.items():
            if isinstance(name, tuple):
                (param_name, nth, attr_name) = name
                self.add_sub_parameter(param_name, nth, attr_name, value)
            else:
                self.add_parameter(name, value)

    def add_parameter(self, name, value):
        setattr(self, name, value)
        self._parameter_names.append(name)

    def add_sub_parameter(self, param_name, nth, attr_name, value):
        if not hasattr(self, param_name):
            setattr(self, param_name, [])
            self._parameter_names.append(param_name)
            self._subblock_names.append(param_name)
        sub_blocks = getattr(self, param_name)
        if nth > len(sub_blocks):
            sub_blocks.append(SubBlock(param_name, nth))
        sub_blocks[nth - 1].add_parameter(attr_name, value)

    def __eq__(self, other):
        if type(other) != type(self):
            return False
        return self.__dict__ == other.__dict__

    def __str__(self):
        output = StringIO()
        output.write(f'&{self._name}\n')
        for name in self._parameter_names:
            value = getattr(self, name)
            if name in self._subblock_names:
                for sub_block in value:
                    output.write(str(sub_block))
            else:
                out_value = format_value(value)
                output.write(f' {name} = {out_value}\n')
        output.write('&end\n')
        return output.getvalue()

    def __repr__(self):  # pragma: no cover
        return self.__str__()


class Input:
    def __init__(self, f):
        self._blocks = []
        block_info = parse_block(f)
        while block_info is not None:
            block_name, block_params = block_info
            block = Block(block_name, block_params)
            setattr(self, block_name, block)
            self._blocks.append(block)
            block_info = parse_block(f)
        if not self._blocks:
            raise ValueError('No input blocks defined')

    def __eq__(self, other):
        if type(other) != type(self):
            return False
        return self.__dict__ == other.__dict__

    def __str__(self):
        output = StringIO()
        for ix, block in enumerate(self._blocks):
            if ix > 0:
                output.write('\n')
            output.write(str(block))
        return output.getvalue()

    def __repr__(self):  # pragma: no cover
        return self.__str__()

    def write(self, f):
        """
        Write the contents of this block to a file.

        :param f: The destination file (or file-like) object.
        """
        f.write(str(self))

    def write_to(self, filename):
        """
        Write the contents of this block to a file.

        :param filename: The destination file name.
        """
        with open(filename, 'w') as f:
            self.write(f)

    def copy(self):
        return copy.deepcopy(self)


def read_input(f):
    """
    Read a model input file and return an `Input` instance.

    :param f: A file-like object from which to read the parameter values.
    """
    return Input(f)


def load_input(filename):
    """
    Read a model input file and return an `Input` instance.

    :param filename: The filename of the model input file.
    """
    with open(filename) as f:
        sim = read_input(f)
    return sim
