import importlib.resources
from pathlib import Path


def n_plus_12C_files():
    """The files required for the 'n+12C' example simulation."""
    return [
        'AMDC-masses.dat',
        'n+12C.inp',
        'n+12C.sh',
        'n+12C.toml',
        'n_plus_12C.py',
    ]


def _save_files(files, out_dir):
    """
    Save the files needed to run am example simulation.

    :param files: A list of file names.
    :param out_dir: The directory in which to save the files (optional; the
        default value is the working directory).
    """
    if out_dir is None:
        out_dir = Path.cwd()
    else:
        out_dir = Path(out_dir).expanduser().resolve()

    for filename in files:
        out_file = out_dir / filename
        contents = importlib.resources.read_binary(
            package='mcasopt.example', resource=filename
        )
        with open(out_file, 'wb') as f:
            f.write(contents)


def _remove_files(files, out_dir):
    """
    Remove the files needed to run an example simulation.

    :param files: A list of file names.
    :param out_dir: The directory that contains the files (optional; the
        default value is the working directory).

    :raise FileNotFoundError: if any example files are missing.
    """
    if out_dir is None:
        out_dir = Path.cwd()
    else:
        out_dir = Path(out_dir).expanduser().resolve()

    for filename in files:
        out_file = out_dir / filename
        out_file.unlink()


def save_n_plus_12C_files(out_dir=None):
    """
    Save the files needed to run the 'n+12C' example simulation.

    :param out_dir: The directory in which to save the files (optional; the
        default value is the working directory).
    """
    _save_files(n_plus_12C_files(), out_dir)


def remove_n_plus_12C_files(out_dir=None):
    """
    Remove the files needed to run the 'n+12C' example simulation.

    :param out_dir: The directory that contains the files (optional; the
        default value is the working directory).

    :raise FileNotFoundError: if any example files are missing.
    """
    _remove_files(n_plus_12C_files(), out_dir)
