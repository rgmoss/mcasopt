# Settings for the Sphinx documentation.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = mcasopt
SOURCEDIR     = doc/src
BUILDDIR      = doc/out

# Default target: display usage information.
help:
	@echo
	@echo "Run 'make html' to generate HTML documentation for mcasopt"
	@echo "This documentation will be located in $(BUILDDIR)/html"
	@echo

# Build the HTML documentation.
html: Makefile
	@$(SPHINXBUILD) -M html "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS)

.PHONY: html
