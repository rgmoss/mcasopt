mcasopt - Optimisation package for MCAS
=======================================

.. Note: don't include the title from README.rst.

.. include:: ../../README.rst
   :start-line: 2

.. note:: This documentation should be the complete reference to using and
   modifying mcasopt_, but it is currently a work in progress. If you want to
   contribute to the documentation, or report problems (such as missing or
   incomplete content), please file an issue on the
   `issue tracker <https://bitbucket.org/robmoss/mcasopt/issues>`__.

.. toctree::
   :hidden:

   Home <self>

.. toctree::
   :maxdepth: 2
   :caption: Getting started

   tutorial/installation
   tutorial/quickstart

.. toctree::
   :maxdepth: 2
   :caption: Introduction to MCAS

   mcas/index
   mcas/running
   mcas/parameters
   mcas/output
   mcas/mcasopt
   mcas/metric

.. toctree::
   :maxdepth: 2
   :caption: High Performance Computing

   hpc/spartan
   hpc/slurm
   hpc/gadi

.. toctree::
   :maxdepth: 2
   :caption: The mcasopt package

   pkg/overview
   pkg/scripting
   pkg/api
   pkg/tests

.. toctree::
   :maxdepth: 2
   :caption: Contributing to mcasopt

   pkg/contrib
   pkg/docs
   pkg/license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
