.. _installation:

Installation
============

Requirements
------------

The mcasopt_ package is made available in a public repository on
`Bitbucket <https://bitbucket.org/>`__.
To download and install it, you need the following software:

* :ref:`MCAS <MCAS>`;

* `Python <https://www.python.org/>`__ version 3.8 or later; and

* `Git <https://git-scm.com/>`__.

Create a Python virtual environment
-----------------------------------

It's best to install mcasopt_ in a Python `virtual environment <https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment>`__, so that it is isolated from other projects.

.. note:: You can create the virtual environment wherever you prefer.
   In the instructions below, the virtual environment is located at ``~/venv``.

You can create a virtual environment with the following steps:

1. Connect to Spartan.

   .. code-block:: shell

      ssh spartan.hpc.unimelb.edu.au

2. Load the required Spartan modules:

   .. code-block:: shell

      module purge
      module load foss
      module load SciPy-bundle

3. Create the virtual environment:

   .. code-block:: shell

      cd
      python3 -m venv ~/venv

4. Activate the virtual environment:

   .. code-block:: shell

      source ~/venv/bin/activate

   .. note:: Use your chosen location, rather than ``~/venv``.

5. Ensure that the ``pip`` and ``setuptool`` packages are up to date:

   .. code-block:: shell

      pip install --upgrade pip setuptools

6. When you are not using the virtual environment, you can deactivate it:

   .. code-block:: shell

      deactivate

Install from source
-------------------

To install mcasopt_ on Spartan, follow these steps.

1. Connect to Spartan.

   .. code-block:: shell

      ssh spartan.hpc.unimelb.edu.au

2. Load the required Spartan modules:

   .. code-block:: shell

      module purge
      module load foss
      module load SciPy-bundle

3. Activate the virtual environment:

   .. code-block:: shell

      source ~/venv/bin/activate

   .. note:: Use your chosen location, rather than ``~/venv``.

5. Download the mcasopt_ source code.
   This will create a new directory called ``mcasopt``.

   .. code-block:: shell

      git clone https://bitbucket.org/robmoss/mcasopt.git

6. Enter the ``mcasopt`` directory and install the package.

   .. code-block:: shell

      cd mcasopt
      pip install -e .

Check that mcasopt is installed
-------------------------------

You can check whether mcasopt_ has been installed correctly by trying to load it from within the Python interpreter.

1. Launch the Python interpreter.

   .. code-block:: shell

      python3

2. Attempt to load mcasopt_. This should produce **no output**.

   .. code-block:: python

      import mcasopt

3. Exit the Python interpreter.

   .. code-block:: python

      exit()

Update from source
------------------

As updates to mcasopt_ are made, you may wish to upgrade the version that you have installed.

1. Connect to Spartan.

   .. code-block:: shell

      ssh spartan.hpc.unimelb.edu.au

2. Enter the ``mcasopt`` directory and download updates to the package.

   .. code-block:: shell

      cd mcasopt
      git pull
