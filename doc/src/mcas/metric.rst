.. _mcas-metric:

The error metric (objective function)
=====================================

Functional form
---------------

The error metric, or objective function, :math:`f(\mathbf{x})` is defined in
terms of :math:`K` criteria, each of which specifies the desired attributes of
a bound or resonance state:

* The weight :math:`w_k` scales errors in this criterion, relative to other
  criteria.

* The energy :math:`E_k`.

* The set of possible spins :math:`\{J_k^\pi\}`.

* The scaling factor :math:`h_k` for the half-width error, relative to the
  energy error; this can be set to zero if there is no half-width.

* The half-width :math:`\Gamma_k`.

The objective function is the weighted sum of the difference between the
criteria and the MCAS output states.

.. math::
   :nowrap:

   \begin{align}
   f(\mathbf{x}) &= \sum_{k=1}^K w_k \cdot \left[f_E(E_{mcas}, E_k)
     + h_k \cdot f_\Gamma(\Gamma_{mcas}, \Gamma_K) \right]
     + f_J(J_{mcas}^\pi, \{J_k^\pi\}) \\
   f_E(E_{mcas}, E_k) &= \lvert E_{mcas} - E_k \rvert \\
   f_\Gamma(\Gamma_{mcas}, \Gamma_k) &= \lvert \Gamma_{mcas} - \Gamma_k \rvert \\
   f_J(J_{mcas}^\pi, \{J_k^\pi\}) &= \begin{cases}
      0 & \text{if } J_{mcas}^\pi \in \{J_k^\pi\} \\
      10^6 & \text{otherwise}
      \end{cases}
   \end{align}

.. note:: There is a very large penalty for states with incorrect spin, which
   should effectively exclude any solutions from including an incorrect spin.

Limitations of experimental data
--------------------------------

For resonance states, available experimental data may comprise energies and
widths, while for bound states only energies are available.
Width measurements are limited by experimental precision, and are typically
binned.
Sufficiently thin resonances can occur within a single bin and are designated
to have width equivalent to that of the entire bin, which represents an
**upper limit** for :math:`\Gamma` rather than being a precise value.

.. note:: It may also be desirable to specify an upper bound for :math:`E`
   rather than a precise value.

Intruder states are states whose energies have not been measured in an
experiment.
The model may output energies that do not match any of the provided criteria;
if it's a low-spin orbit then it might be a previously-undetected state, if
it's a high-spin orbit then it's likely a mistake in the model and should be
ignored.
In either case, the model *should not be penalised*.

.. note:: Clarify that the calculation/output should not be penalised.
   It's not apparent from the functional form, as described above.

Selecting appropriate states for comparison
-------------------------------------------

Each criterion :math:`k \in \{1, 2, \dots, K\}` must be paired with a state
from the MCAS output in order to evaluate the objective function
:math:`f(\mathbf{x})`.
This pairing must respect the following properties:

* There should be no states below the lowest measured bound state (the ground
  state).

* States should only be paired if they have matching spin parities.
  Note that this **does not** necessarily mean there is a unique set of paired
  states, because the set :math:`\{J_k^\pi\}` may (a) be empty; or (b) contain
  multiple elements.

.. seealso:: Figures 1 and 7 in the `Phys Rev C MCAS paper
   <http://dx.doi.org/10.1103/PhysRevC.95.034305>`__ demonstrate how MCAS
   results have been compared to experimental data.

Currently, two pairing strategies have been discussed, both of which iterate
over the criteria from lowest-energy to highest-energy:

* "First state": match each criterion with the lowest-energy state that (a)
  has a matching spin parity; and (b) has not already been paired with a
  criterion.

* "Nearest state": match each criterion with the state with the nearest energy
  that (a) has a matching spin parity; and (b) has not already been paired
  with a criterion.

  * This strategy must still pair the lowest-energy criterion with the
    lowest-energy state that has a matching spin parity.

.. note:: We may also want to include *optional* states when using the "first
   state" strategy, to represent states that haven't been measured but which
   are suspected to exist.
