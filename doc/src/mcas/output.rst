Model output
============

The output is a (sorted) list of bound and resonance states.
Each state comprises:

* An energy :math:`E`;

* A spin :math:`J^\pi`; and

* A half-width :math:`\Gamma`.

Positive energies are kinetic (resonance states), negative energies are bound
states.
The output is sorted from lowest negative energy to highest positive energy.
