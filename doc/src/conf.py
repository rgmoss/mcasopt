# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

import os.path
import sys

# NOTE: this is important, must set the path correctly.
sys.path.insert(0, os.path.abspath('../../src'))

# Avoid build errors on Read The Docs (requires Sphinx 1.3 or newer).
# See http://read-the-docs.readthedocs.io/en/latest/faq.html for details.
autodoc_mock_imports = ['numpy', 'scipy', 'scipy.optimize', 'tomli']

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.mathjax',
    'sphinx_rtd_theme',
]

autodoc_default_options = {
    'members': True,
    'member-order': 'bysource',
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = 'mcasopt'
copyright = '2018-2023, Rob Moss'
author = 'Rob Moss'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '0.1'
# The full version, including alpha/beta/rc tags.
release = '0.1'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = 'en'

# Define common RST content, such as the link to the master repository.
rst_epilog = """
.. _Gadi environment modules: https://opus.nci.org.au/display/Help/Environment+Modules
.. _Gadi queues: https://opus.nci.org.au/display/Help/Queue+Structure
.. _Gadi quick reference: https://opus.nci.org.au/display/Help/Gadi+Quick+Reference+Guide
.. _Gadi user guide: https://opus.nci.org.au/display/Help/Gadi+User+Guide
.. _mcasopt: http://bitbucket.org/robmoss/mcasopt
.. _NCI documentation: https://opus.nci.org.au/
.. _NCI Python documentation: https://opus.nci.org.au/display/Help/Python
.. _Slurm documentation: https://slurm.schedmd.com/
.. _Spartan documentation: https://dashboard.hpc.unimelb.edu.au/
.. _Spartan FAQ: https://dashboard.hpc.unimelb.edu.au/faq/
.. |br| raw:: html

   <br />
"""

# If True, figures, tables and code-blocks are automatically numbered if they
# have a caption. The numref role is enabled. Obeyed so far only by HTML and
# LaTeX builders. Default is False.
numfig = True

# A dictionary mapping 'figure', 'table', 'code-block' and 'section' to
# strings that are used for format of figure numbers. As a special character,
# %s will be replaced to figure number.
numfig_format = {
    'figure': 'Figure %s',
    'table': 'Table %s',
    'code-block': 'Listing %s',
    'section': 'Section %s',
}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This patterns also effect to html_static_path and html_extra_path
exclude_patterns = [
    '_build',
    'Thumbs.db',
    '.DS_Store',
    '*.aux',
    '*.tex',
    '*.pdf',
    'Makefile',
]

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = False


# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'sphinx_rtd_theme'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
# html_theme_options = {}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
