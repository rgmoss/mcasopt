Writing documentation
=====================

This documentation is written in `reStructured Text`_, formatted by `Sphinx`_,
and published online by `Read the Docs`_.

Authoring
---------

The source for all of the documentation is located in the ``doc/src``
directory, located within the ``mcasopt`` directory.
**If you are not familiar with reStructured Text**, then see `this reference
<reStructured Text_>`_ for an introduction to the concepts and syntax.

Building
--------

To build the documentation, follow these steps:

1. Create a Python virtual environment:

   .. code-block:: shell

      python3 -m venv docs_venv

2. Activate this virtual environment and install `nox <https://nox.thea.codes/>`__:

   .. code-block:: shell

      . ./docs_venv/bin/activate
      pip install nox

3. Build the documentation:

   .. code-block:: shell

      nox -s docs


The documentation will be published to ``doc/out/`` and can be viewed in a
browser.
For example:

.. code-block:: shell

   firefox doc/out/index.html &

.. _reStructured Text: http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
.. _Sphinx: http://www.sphinx-doc.org/
.. _Read the Docs: https://readthedocs.org/
