.. _mcasopt-overview:

Overview of mcasopt
===================

The mcasopt_ package acts as an interface between :ref:`MCAS <MCAS>` and the numerical optimisation solvers provided by `scipy.optimize.minimize <https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html>`__.
It uses :ref:`Slurm <Slurm>` to run MCAS simulations on the :ref:`Spartan`, and wraps this in a standard Python function that can be passed to the solver.

How to use mcasopt
------------------

#. Make sure you have a copy of the file ``"AMDC-masses.dat"``; and

#. Define a starting MCAS :ref:`parameters file <mcas-params>`, which we will call ``"n+12C.inp"``;

#. Write a Python function that returns the MCAS input for a given parameter vector ``x``, in a file that we will call ``n_plus_12C.py``;

   .. literalinclude:: ../../../src/mcasopt/example/n_plus_12C.py
      :language: python
      :linenos:
      :name: example-py
      :caption: An example mcasopt_ input function.

#. Define all of the mcasopt_ settings and the **compound states** in a TOML file, which we will call ``"n+12C.toml"`` (see the example below).

   .. literalinclude:: ../../../src/mcasopt/example/n+12C.toml
      :language: toml
      :linenos:
      :name: example-toml
      :caption: An example mcasopt_ settings file.

.. note::  See the next section, :ref:`mcasopt-spartan`, for instructions on how to run mcasopt_ on Spartan.

What mcasopt does
-----------------

1. Defines a function that takes a parameter vector :math:`x = [x_1 \ x_2 \ ... \ x_N]` and:

   (a) Use the MCAS input function :math:`f` (in this case, defined in :ref:`the example script <example-py>`) to write an MCAS input file for :math:`f(x)`.

   (b) Launches an MCAS simulation for each of this input file, using the Slurm settings (in this case, defined in lines 17-24 of :ref:`settings file <example-toml>`);

   (c) Waits until the simulation has finished; and

   (d) Returns the measure :math:`g(x)` of how well the MCAS simulation at location :math:`x` matches the optimisation criteria defined in the :ref:`settings file <example-toml>` (lines 26-45).

2. Calls the solver, providing the function created in step #1, starting from the initial parameter vector ``x_0`` (line 7 of :ref:`the settings file <example-toml>`), and using the solver settings (lines 10-15 of the :ref:`settings file <example-toml>`).

What the solver does
--------------------

The solver will perform the following loop:

1. Evaluate the function at the current location :math:`x^k`.
   This launches an MCAS simulation (as described above), in order to evaluate the goodness of fit :math:`g(x^k)`.

2. When the MCAS simulation launched in step #1 has completed, the solver will either:

   + Accept :math:`x^k` as the approximate solution, and return :math:`x^k`; or

   + Search for a new point :math:`x^{k+1}` at which to evaluate the function, and return to step #1.
