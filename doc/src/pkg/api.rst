API documentation
=================

Running the optimiser
---------------------

The optimiser accepts three arguments:

1. The :class:`~mcasopt.solver.Evaluator` that determines how to evaluate the target function ``f(x)``;

2. The starting location ``x_0``; and

3. The :class:`~mcasopt.solver.Solver` configuration settings.

.. autofunction:: mcasopt.minimise

Function evaluation
-------------------

The mcasopt_ package provides several "evaluators", which can be used to evaluate different types of target functions:

* The :class:`~mcasopt.solver.FunctionEvaluator` evaluates standard Python functions;

* The :class:`~mcasopt.solver.ExpressionEvaluator` evaluated numeric Python expressions; and

* The :class:`~mcasopt.run.McasEvaluator` runs MCAS simulations and scores the outputs against the solution criteria.

.. note::

   :class:`~mcasopt.run.McasEvaluator` requires a :class:`~mcasopt.run.Runner`, which defines how to run MCAS simulations.
   The following runners are provided by mcasopt_:

   * :class:`~mcasopt.run.SlurmRunner`, which runs MCAS simulations on Spartan via the Slurm job scheduling system;

   * :class:`~mcasopt.run.LocalRunner`, which runs MCAS simulations on your local computer; and

   * :class:`~mcasopt.run.FakeMcasRunner`, which runs fake MCAS simulations on the local computer by calling :func:`mcasopt.run.fake_mcas`.

.. autoclass:: mcasopt.solver.Evaluator

.. autoclass:: mcasopt.solver.FunctionEvaluator

.. autoclass:: mcasopt.solver.ExpressionEvaluator

.. autoclass:: mcasopt.run.McasEvaluator
   :exclude-members: start, finished, result

.. autofunction:: mcasopt.run.fake_mcas

Running commands
----------------

.. autoclass:: mcasopt.run.Command

.. autoclass:: mcasopt.run.Runner

.. autoclass:: mcasopt.run.SlurmRunner

.. autoclass:: mcasopt.run.LocalRunner

.. autoclass:: mcasopt.run.FakeMcasRunner

Configuration settings
----------------------

The configuration settings :class:`~mcasopt.run.Config` are divided into three sections:

* The :class:`~mcasopt.run.Experiment` definition;

* The :class:`~mcasopt.solver.Solver` settings; and

* The :class:`~mcasopt.run.Slurm` settings.

These settings can be loaded from an external file with :func:`~mcasopt.run.load_config`.

.. autofunction:: mcasopt.run.load_config

.. autoclass:: mcasopt.run.Config

.. autoclass:: mcasopt.run.Experiment

.. autoclass:: mcasopt.solver.Solver

.. autoclass:: mcasopt.run.Slurm

Model criteria
--------------

.. autofunction:: mcasopt.criteria.parse_criteria

.. autoclass:: mcasopt.criteria.Criteria

Model parameters
----------------

.. autofunction:: mcasopt.model.load_input

.. autoclass:: mcasopt.model.Input

Caching simulation results
--------------------------

.. autofunction:: mcasopt.cache.load_cache

.. autoclass:: mcasopt.cache.Cache

Example experiments
-------------------

n+12C
^^^^^

.. autofunction:: mcasopt.example.save_n_plus_12C_files

.. autofunction:: mcasopt.example.remove_n_plus_12C_files

.. autofunction:: mcasopt.example.n_plus_12C_files
