#
# Generate PNG images for the online documentation.
#
# For each PNG image, there is a corresponding LaTeX file.
#

TEX_FILES := $(wildcard *.tex)
PDF_FILES := $(TEX_FILES:.tex=.pdf)
PNG_FILES := $(TEX_FILES:.tex=.png)

LATEX := xelatex
LATEX_FLAGS := -interaction=nonstopmode -halt-on-error

CONVERT := convert
CONVERT_FLAGS := -density 150

TMP_EXTS := aux log out
TMP_FILES := $(foreach EXT,$(TMP_EXTS),$(wildcard *.$(EXT)))

# Default action: generate a PNG image from each LaTeX file.
default: $(PNG_FILES)

# Remove temporary files.
clean:
	@rm -f $(TMP_FILES)

# Remove generated PNG images.
distclean: clean
	@rm -f $(PNG_FILES)

# Convert each PDF document into a PNG image.
%.png: %.pdf
	@$(CONVERT) $(CONVERT_FLAGS) $< $@

# Compile each LaTeX file into a separate PDF document.
%.pdf: %.tex
	@$(LATEX) $(LATEX_FLAGS) $<
	@$(LATEX) $(LATEX_FLAGS) $<

# Identify targets that are not file names.
.PHONY: default clean distclean
