.. _Spartan:

Spartan HPC system
==================

Refer to the `Spartan documentation`_ and the `Spartan FAQ`_ for more details.

Spartan provides a number of Python modules.
The ``SciPy-bundle`` modules are ideal for mcasopt_ because they include all of the required Python libraries.
You can search for available ``SciPy-bundle`` modules with the following command:

.. code-block:: shell

   module spider SciPy-bundle

The contents of a particular module can be viewed using ``module spider``:

.. code-block:: shell

   module spider SciPy-bundle/2022.05

You can load a particular module with ``module load``:

.. code-block:: shell

   module load SciPy-bundle/2022.05

See the `Getting Started guide <https://dashboard.hpc.unimelb.edu.au/started/>`__ for an introduction to submitting and managing jobs on Spartan.
